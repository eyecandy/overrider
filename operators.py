import bpy

import re
from typing import Any, Dict, Tuple

from . import override_keys


#######################################
######         Utilities        #######
#######################################


# Mapping from `bl_rna.properties['propname'].XXXX` to their equivalent
# id_datablock['_RNA_UI']['propname']['YYYY'].
_bl_rna_to_rna_ui = {
    'hard_min': 'min',
    'hard_max': 'max',
    'soft_min': 'soft_min',
    'soft_max': 'soft_max',
    'subtype': 'subtype',
}

_bl_rna_type_specific_defaults = {
    'BOOLEAN': {
        'min': 0,
        'max': 1,
        'soft_min': 0,
        'soft_max': 1,
    },
}

def _get_default_rna_ui(rna_prop_type: str) -> Dict:
    """Construct the default _RNA_UI dict for the given property type.

    Always returns a new dictionary.
    """

    rna_ui = {}

    # Set some defaults first. For example, a Boolean property doesn't have a
    # 'min' value, but this should be set in _RNA_UI anyway.
    try:
        type_specific_defaults = _bl_rna_type_specific_defaults[rna_prop_type]
    except KeyError:
        pass
    else:
        rna_ui.update(type_specific_defaults)

    return rna_ui


def _get_property_info(rna_path: str) -> Tuple[Any, Dict]:
    """Given an RNA path to a property, returns info about that property.

    Uses the RNA information in `bl_rna['property_name']` to construct a
    dictionary that is suitable for use in custom properties' `_RNA_UI`.

    :return: tuple (current value, _RNA_UI dict)
    """

    object_path, prop_name = rna_path.rsplit('.', 1)
    py_object = eval(object_path)
    current_value = getattr(py_object, prop_name)

    bl_rna = py_object.bl_rna.properties[prop_name]
    rna_ui = _get_default_rna_ui(bl_rna.type)

    # Copy existing settings from the RNA property definition.
    for bl_rna_attr, rna_ui_key in _bl_rna_to_rna_ui.items():
        try:
            rna_ui[rna_ui_key] = getattr(bl_rna, bl_rna_attr)
        except AttributeError:
            # Not all property types have the same attributes.
            pass

    if getattr(bl_rna, 'is_array', False):
        rna_ui['default'] = tuple(bl_rna.default_array)
        current_value = tuple(current_value)
    else:
        rna_ui['default'] = bl_rna.default

    return current_value, rna_ui


def _ensure_properties_for_overrides_exists_on(datablock):
    """Ensure that _RNA_UI and _overrides_rna_paths exists."""

    if '_RNA_UI' not in datablock:
        datablock['_RNA_UI'] = {}

    if '_overrides_rna_paths' not in datablock:
        datablock['_overrides_rna_paths'] = {}
        datablock['_RNA_UI']['_overrides_rna_paths'] = {
            'description': 'RNA paths of the overrides',
        }


def _add_override(datablock, override_key: str, rna_path: str):
    """Store an override for the rna_path on the datablock.

    Typically the datablock is the current scene, so that it can be
    iterated over to construct a user interface, write the desired
    overrides to a text datablock, etc.
    """

    current_value, rna_ui = _get_property_info(rna_path)
    if override_key not in datablock:
        datablock[override_key] = current_value

    _ensure_properties_for_overrides_exists_on(datablock)
    datablock['_RNA_UI'][override_key] = rna_ui
    datablock['_overrides_rna_paths'][override_key] = rna_path

def _remove_override(datablock, override_key: str):
    """removes an override for the specified key in override_key"""

    if override_key in datablock:
        del datablock['_RNA_UI'][override_key]
        del datablock['_overrides_rna_paths'][override_key]
        del datablock[override_key]


def _generate_expression(rna_path: str, rna_ui) -> str:

    # in case the property is a color, it has a data type IDPropertyArray
    # I can't figure out how to test for it, so let's just test if it's NOT IDPropertyArray

    if type(rna_ui) is float or type(rna_ui) is int or type(rna_ui) is bool:
        pass
    elif type(rna_ui) is str:
        rna_ui = "".join(rna_ui)
        rna_ui = f"\"{rna_ui}\""
    else:
        rna_ui = list(rna_ui)

    return(f"{rna_path} = {rna_ui}")

#######################################
######     Blender Operators    #######
#######################################


class OR_OT_addOverride(bpy.types.Operator):
    """Adds an operator to the scene scene properties"""
    bl_idname = "overrider.add_override"
    bl_label = "Add scene override"

    #override_key: bpy.props.StringProperty(name="Name of override", default="")
    rna_path: bpy.props.StringProperty(name="Data path to override", default="")

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def execute(self, context):
        override_key = override_keys.generate(self.rna_path)

        #For now, let's add a _ if key already exists as a way of preventing overlaps
        if override_key in context.scene:
            self.report({'ERROR'}, "This Override already exists")

        _add_override(context.scene, override_key, self.rna_path)

        return{'FINISHED'}


class OR_OT_removeOverride(bpy.types.Operator):
    """Removes the active override from the scene properties"""
    bl_idname = "overrider.remove_override"
    bl_label = "Remove scene override"

    override_key: bpy.props.StringProperty(name="Override to remove", default="")

    def execute(self, context):
        _remove_override(context.scene, self.override_key)

        return{'FINISHED'}


class OR_OT_writeTextData(bpy.types.Operator):
    """Writes the overrides to a text file which is set to register on load. """
    bl_idname = "overrider.writetextdata"
    bl_label = "write Text Datablock"

    def execute(self, context):

        scene = context.scene

        if 'overrides.py' in bpy.data.texts:
            text = bpy.data.texts['overrides.py']
            text.clear()
        else:
            text = bpy.data.texts.new('overrides.py')

        text.use_module = True
        text.write("import bpy \n\n")

        for override in scene['_overrides_rna_paths']:
            rna_path = scene['_overrides_rna_paths'][override]
            rna_ui = scene[override]

            arguments = _generate_expression(rna_path, rna_ui)
            text.write(f"{arguments}\n")

        return {'FINISHED'}



class OR_OT_executeOverride(bpy.types.Operator):
    """Runs all the commands specified in the scene overrides"""
    bl_idname = "overrider.execute_override"
    bl_label = "execute override"

    def execute(self, context):

        scene = context.scene

        for override in scene['_overrides_rna_paths']:

            rna_path = scene['_overrides_rna_paths'][override]
            rna_ui = scene[override]

            arguments = _generate_expression(rna_path, rna_ui)

            print(f"executing Overrides: {arguments}")

            try:
                exec(arguments)
                continue

            except:
                self.report({'ERROR'}, f"Found an error in: {arguments}")
                return{'CANCELLED'}

        return {'FINISHED'}

class OR_OT_addOverrideButton(bpy.types.Operator):
    """Adds an operator on button mouse hover"""
    bl_idname = "overrider.add_override_button"
    bl_label = "Add override on button mouse over"
    bl_options = {'REGISTER'}

    rna_path: bpy.props.StringProperty(name="Data path to override", default="")

    _array_path_re = re.compile(r'^(.*)\[[0-9]+\]$')

    @classmethod
    def poll(cls, context):
        return bpy.ops.ui.copy_data_path_button.poll()

    def invoke(self, context, event):
        bpy.ops.ui.copy_data_path_button(full_path=True)
        rna_path = context.window_manager.clipboard

        # Strip off array indices (f.e. 'a.b.location[0]' -> 'a.b.location')
        m = self._array_path_re.match(rna_path)
        if m:
            rna_path = m.group(1)

        self.rna_path = rna_path

        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def execute(self, context):
        override_key = override_keys.generate(self.rna_path)

        if override_key in context.scene:
            self.report({'ERROR'}, "This Override already exists")

        _add_override(context.scene, override_key, self.rna_path)

        return{'FINISHED'}

classes = (
    OR_OT_addOverride,
    OR_OT_removeOverride,
    OR_OT_executeOverride,
    OR_OT_writeTextData,
    OR_OT_addOverrideButton,
)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)

def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)