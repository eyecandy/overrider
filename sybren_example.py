from typing import Any, Dict, Tuple

import bpy

# Mapping from `bl_rna.properties['propname'].XXXX` to their equivalent
# id_datablock['_RNA_UI']['propname']['YYYY'].
_bl_rna_to_rna_ui = {
    'hard_min': 'min',
    'hard_max': 'max',
    'soft_min': 'soft_min',
    'soft_max': 'soft_max',
    'subtype': 'subtype',
}


def _get_property_info(rna_path: str) -> Tuple[Any, Dict]:
    """Given an RNA path to a property, returns info about that property.

    Uses the RNA information in `bl_rna['property_name']` to construct a
    dictionary that is suitable for use in custom properties' `_RNA_UI`.

    :return: tuple (current value, _RNA_UI dict)
    """

    object_path, prop_name = rna_path.rsplit('.', 1)
    py_object = eval(object_path)
    current_value = getattr(py_object, prop_name)

    bl_rna = py_object.bl_rna.properties[prop_name]
    rna_ui = {}
    for bl_rna_attr, rna_ui_key in _bl_rna_to_rna_ui.items():
        rna_ui[rna_ui_key] = getattr(bl_rna, bl_rna_attr)

    if bl_rna.is_array:
        rna_ui['default'] = tuple(bl_rna.default_array)
        current_value = tuple(current_value)
    else:
        rna_ui['default'] = bl_rna.default

    return current_value, rna_ui


def _ensure_properties_for_overrides_exists_on(datablock):
    """Ensure that _RNA_UI and _overrides_rna_paths exists."""

    if '_RNA_UI' not in datablock:
        datablock['_RNA_UI'] = {}

    if '_overrides_rna_paths' not in datablock:
        datablock['_overrides_rna_paths'] = {}
        datablock['_RNA_UI']['_overrides_rna_paths'] = {
            'description': 'RNA paths of the overrides',
        }


def add_override(datablock, override_key: str, rna_path: str):
    """Store an override for the rna_path on the datablock.

    Typically the datablock is the current scene, so that it can be
    iterated over to construct a user interface, write the desired
    overrides to a text datablock, etc.
    """

    current_value, rna_ui = _get_property_info(rna_path)
    if override_key not in datablock:
        datablock[override_key] = current_value

    _ensure_properties_for_overrides_exists_on(datablock)
    datablock['_RNA_UI'][override_key] = rna_ui
    datablock['_overrides_rna_paths'][override_key] = rna_path

def remove_override(datablock, override_key: str):
    """removes an override for the specified key in override_key"""

    if override_key in datablock:
        del datablock['_RNA_UI'][override_key]
        del datablock['_overrides_rna_paths'][override_key]
        del datablock[override_key]


rna_path = "bpy.data.materials['Suzie'].node_tree.nodes['Principled BSDF'].inputs['Base Color'].default_value"
add_override(bpy.context.scene, 'Suzie Base Color', rna_path)

rna_path = "bpy.data.materials['Suzie'].node_tree.nodes['Principled BSDF'].inputs['Subsurface Color'].default_value"
add_override(bpy.context.scene, 'Suzie SSS Color', rna_path)