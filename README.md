# overrider

This is a simple Blender add-on to simplify overriding values via the Blender python API. 
It generates a text datablock in .blend files which can be used remotely (for example on render farms)