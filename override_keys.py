"""RNA override key generator.

This takes an RNA path and turns it into a human-readable, shorter representation.


Running the doctests:
    blender -b testfile.blend --python override_keys.py

"""

import bpy


def generate(rna_path: str) -> str:
    """Returns simpler override key from RNA path.

    >>> generate('bpy.data.objects["Cube"].color')
    'Cube Color'

    These document the current behaviour, and will change in the future:

    >>> generate('bpy.data.materials["MAT-hailey.body"].node_tree.nodes["Group"].inputs[6].default_value')
    'MAT-hailey Default value'
    >>> generate('bpy.data.materials["MAT-hailey.body"].node_tree.nodes["Mix"].inputs[2].default_value')
    'MAT-hailey Default value'
    """

    split_path = rna_path.rsplit('.')
    # Blender paths usually start with bpy.data...
    # TODO: This automatic naming currently gets messed up if there are "." in the datablock names!!

    if "bpy" not in split_path:
        print(f"Path {rna_path!r} is not a valid Blender ID path")
        return ""

    data_name = split_path[2].split('"')[1]

    #find out if the overridden value is part of a node tree
    if "node_tree" in split_path[3]:
        #get the name of the input node
        input_path = rna_path.rsplit('.', 1)
        id_name = eval(f"{input_path[0]}.name")

    elif "nodes" in split_path[3]:
        id_name = split_path[3].split('"')[1]
    else:
        #if not, simply use the name of the last substring after the .
        split_path = rna_path.rsplit(".", 1)
        id_name =  split_path[1].replace("_", " ").capitalize()

    return f"{data_name} {id_name}"


if __name__ == '__main__':
    import doctest
    result = doctest.testmod()
    print(f"All doctests have run: {result}")
