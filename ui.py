import bpy

class OverridePanel(bpy.types.Panel):
    bl_label = "Scene Overrides"
    bl_idname = "SCENE_PT_overrides"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "scene"

    def draw(self, context):
        layout = self.layout

        scene = context.scene

        row = layout.row()

        row.operator("overrider.add_override", icon='ADD', text="Add new override")

        if '_overrides_rna_paths' in scene:
            row.operator("overrider.execute_override", text="", icon='FILE_REFRESH')

            for override_key in scene['_overrides_rna_paths']:
                box = layout.box()

                split = box.split(factor=0.55)

                split.label(text=override_key)
                row = split.row(align=False)

                row.prop(scene, '["%s"]' % bpy.utils.escape_identifier(override_key) , text="")
                props = row.operator("overrider.remove_override", text="", icon='REMOVE')
                props.override_key = override_key

            row = layout.row()
            row.operator("overrider.writetextdata", text="Write overrides to text data-block", icon='TEXT')

classes = (
    OverridePanel,
)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)

def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)

if __name__ == "__main__":
    register()