import bpy
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "Overrider",
    "author" : "Andy Goralczyk",
    "description" : "",
    "blender" : (2, 80, 0),
    "version" : (0, 0, 1),
    "location" : "",
    "warning" : "",
    "category" : "Workflow"
}

import importlib

from . import ui
from . import operators
from . import override_keys


# allow for simple refresh of addon
modules = (
        operators,
        override_keys,
        ui,
        )

for mod in modules:
    importlib.reload(mod)

def register():
    operators.register()
    ui.register()

    wm = bpy.context.window_manager
    if wm.keyconfigs.addon is not None:
        km = wm.keyconfigs.addon.keymaps.new(name="User Interface")
        kmi = km.keymap_items.new("overrider.add_override_button","O", "PRESS",shift=False, ctrl=False)



def unregister():
    operators.unregister()
    ui.unregister()

if __name__ == "__main__":
    register()